#pragma checksum "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewStart.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7091c65830b0329e613be026ede8a57552863778"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views__ViewStart), @"mvc.1.0.view", @"/Views/_ViewStart.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/_ViewStart.cshtml", typeof(AspNetCore.Views__ViewStart))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp;

#line default
#line hidden
#line 3 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.Models;

#line default
#line hidden
#line 4 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels;

#line default
#line hidden
#line 5 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.PetViewModels;

#line default
#line hidden
#line 6 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.PetOwnerViewModels;

#line default
#line hidden
#line 7 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.ServiceViewModels;

#line default
#line hidden
#line 8 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.AccountViewModels;

#line default
#line hidden
#line 9 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.EventViewModels;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7091c65830b0329e613be026ede8a57552863778", @"/Views/_ViewStart.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"21dfb81fb89037d342781a4dabb0b27d7a9e6059", @"/Views/_ViewImports.cshtml")]
    public class Views__ViewStart : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewStart.cshtml"
  
    Layout = "_Layout";

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
