#pragma checksum "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "05799ce22f1af858252f01f53101741c242476cb"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Pet_Overview), @"mvc.1.0.view", @"/Views/Pet/Overview.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Pet/Overview.cshtml", typeof(AspNetCore.Views_Pet_Overview))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 2 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp;

#line default
#line hidden
#line 3 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.Models;

#line default
#line hidden
#line 4 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels;

#line default
#line hidden
#line 5 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.PetViewModels;

#line default
#line hidden
#line 6 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.PetOwnerViewModels;

#line default
#line hidden
#line 7 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.ServiceViewModels;

#line default
#line hidden
#line 8 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.AccountViewModels;

#line default
#line hidden
#line 9 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\_ViewImports.cshtml"
using VetMedicApp.ViewModels.EventViewModels;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"05799ce22f1af858252f01f53101741c242476cb", @"/Views/Pet/Overview.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"21dfb81fb89037d342781a4dabb0b27d7a9e6059", @"/Views/_ViewImports.cshtml")]
    public class Views_Pet_Overview : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ListOfPetsViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "PetOwner", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("color:black"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Pet", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Export", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("font-size:medium"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-dark btn-sm"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(28, 749, true);
            WriteLiteral(@"<h3><i class=""fa fa-table fa-fw"" aria-hidden=""true""></i>Pregled ljubimaca</h3>
<hr />

<div class=""card mb-3"">
    <div class=""card-header"">
        <i class=""fas fa-paw""></i>
        <strong>Ljubimci</strong>
    </div>
    <div class=""card-body"">
        <div class=""table-responsive"">
            <table class=""table table-bordered"" id=""dataTable"" cellspacing=""0"">
                <thead>
                    <tr>
                        <th>RB</th>
                        <th>Ime</th>
                        <th>Vrsta</th>
                        <th>Rasa</th>
                        <th>Vlasnik</th>
                        <th>Detaljnije</th>
                    </tr>
                </thead>
                <tbody>
");
            EndContext();
#line 24 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                      int i = 0;

#line default
#line hidden
            BeginContext(812, 20, true);
            WriteLiteral("                    ");
            EndContext();
#line 25 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                     foreach (Pet p in Model.Pets)
                    {
                        i = @i + 1;
                        

#line default
#line hidden
            BeginContext(948, 38, true);
            WriteLiteral("<tr>\r\n                            <td>");
            EndContext();
            BeginContext(987, 1, false);
#line 29 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                           Write(i);

#line default
#line hidden
            EndContext();
            BeginContext(988, 39, true);
            WriteLiteral("</td>\r\n                            <td>");
            EndContext();
            BeginContext(1028, 6, false);
#line 30 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                           Write(p.Name);

#line default
#line hidden
            EndContext();
            BeginContext(1034, 39, true);
            WriteLiteral("</td>\r\n                            <td>");
            EndContext();
            BeginContext(1074, 18, false);
#line 31 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                           Write(p.PetType.TypeName);

#line default
#line hidden
            EndContext();
            BeginContext(1092, 39, true);
            WriteLiteral("</td>\r\n                            <td>");
            EndContext();
            BeginContext(1132, 6, false);
#line 32 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                           Write(p.Race);

#line default
#line hidden
            EndContext();
            BeginContext(1138, 39, true);
            WriteLiteral("</td>\r\n                            <td>");
            EndContext();
            BeginContext(1177, 148, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "43e73c820d5f49dc9df3cde325f6e4c2", async() => {
                BeginContext(1266, 17, false);
#line 33 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                                                                                                                   Write(p.Owner.FirstName);

#line default
#line hidden
                EndContext();
                BeginContext(1283, 1, true);
                WriteLiteral(" ");
                EndContext();
                BeginContext(1285, 16, false);
#line 33 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                                                                                                                                      Write(p.Owner.LastName);

#line default
#line hidden
                EndContext();
                BeginContext(1301, 2, true);
                WriteLiteral(" (");
                EndContext();
                BeginContext(1304, 16, false);
#line 33 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                                                                                                                                                         Write(p.Owner.UserName);

#line default
#line hidden
                EndContext();
                BeginContext(1320, 1, true);
                WriteLiteral(")");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            BeginWriteTagHelperAttribute();
            WriteLiteral("Details/");
#line 33 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                                                                     WriteLiteral(p.Owner.UserName);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-action", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1325, 41, true);
            WriteLiteral("</td>\r\n                            <td><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 1366, "\"", 1389, 2);
            WriteAttributeValue("", 1373, "Details\\", 1373, 8, true);
#line 34 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
WriteAttributeValue("", 1381, p.PetId, 1381, 8, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(1390, 134, true);
            WriteLiteral(" style=\"font-size:medium; margin:-15px 0px -10px 0px\" class=\"btn btn-dark btn-sm\">Detaljnije</a></td>\r\n                        </tr>\r\n");
            EndContext();
#line 36 "D:\Djole\Projects Visual Studio\vetmedicapp\VetMedicApp\Views\Pet\Overview.cshtml"
                    }

#line default
#line hidden
            BeginContext(1547, 122, true);
            WriteLiteral("                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n    <div class=\"card-footer small text-muted\">");
            EndContext();
            BeginContext(1669, 164, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d3aeca112d3b437bac915e212e869b6f", async() => {
                BeginContext(1766, 63, true);
                WriteLiteral("<i class=\"fa fa-download fa-sm\" aria-hidden=\"true\"></i> Eksport");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1833, 14, true);
            WriteLiteral("</div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ListOfPetsViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
