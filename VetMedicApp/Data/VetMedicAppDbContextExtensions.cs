﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Linq;
using VetMedicApp.Models;

namespace VetMedicApp.Data
{
    public static class VetMedicAppDbContextExtensions
    {
        public static bool AllMigrationsApplied(this VetMedicAppDbContext context)
        {
            var applied = context.GetService<IHistoryRepository>()
                .GetAppliedMigrations()
                .Select(m => m.MigrationId);

            var total = context.GetService<IMigrationsAssembly>()
                .Migrations
                .Select(m => m.Key);

            return !total.Except(applied).Any();
        }

        public static VetMedicAppDbContext EnsureDatabaseSeeded(this VetMedicAppDbContext context)
        {
            if (!context.Roles.Any())
            {
                seedRolesTable(context);
            }

            if (!context.Users.Any())
            {
                seedUsersTable(context);
            }

            if (!context.UserRoles.Any())
            {
                seedUserRolesTable(context);
            }
            if (!context.PetTypes.Any())
            {
                seedPetTypesTable(context);
            }
            if (!context.Services.Any())
            {
                seedServicesTable(context);
            }

            return context;
        }

        private static void seedRolesTable(VetMedicAppDbContext context)
        {
            context.Roles.AddRange(
                new IdentityRole() { Id = "1", Name = "admin" },
                new IdentityRole() { Id = "2", Name = "petowner" }
                );

            context.SaveChanges();
        }

        private static void seedUserRolesTable(VetMedicAppDbContext context)
        {
            context.UserRoles.AddRange(
                new IdentityUserRole<string>() { UserId = "1", RoleId = "1" },
                new IdentityUserRole<string>() { UserId = "2", RoleId = "2" },
                new IdentityUserRole<string>() { UserId = "3", RoleId = "2" },
                new IdentityUserRole<string>() { UserId = "4", RoleId = "2" },
                new IdentityUserRole<string>() { UserId = "5", RoleId = "2" },
                new IdentityUserRole<string>() { UserId = "6", RoleId = "2" },
                new IdentityUserRole<string>() { UserId = "7", RoleId = "2" },
                new IdentityUserRole<string>() { UserId = "8", RoleId = "2" }
            );

            context.SaveChanges();
        }

        private static void seedUsersTable(VetMedicAppDbContext context)
        {
            Admin admin = new Admin() { Id = "1", AccessFailedCount = 0, ConcurrencyStamp = "b58d0834-4428-4b8b-8585-54f91a26ba71", Email = "admin@glogy.com", EmailConfirmed = true, LockoutEnabled = true, LockoutEnd = null, NormalizedEmail = "admin@glogy.com", NormalizedUserName = "admin", PasswordHash = "AQAAAAEAACcQAAAAEHBL8bFd9GJAYlw8zQiyUsq4Qg+7/1XOo3XBQwUwx50VIlJcoqi8GgrMwGF5DzHFiw==", PhoneNumber = "", PhoneNumberConfirmed = false, SecurityStamp = "ac1d41a1-6a1b-43d5-83d0-1eb64223e354", TwoFactorEnabled = false, UserName = "admin" };
            PetOwner p1 = new PetOwner() { Id = "2", RegistrationDate = DateTime.Now, FirstName = "Marko", LastName = "Tomić", PhoneNumber = "0642345672", StreetAdress = "Zaplanjska 51", Municipality = "Voždovac", AccessFailedCount = 0, ConcurrencyStamp = "28861696-11f7-41f1-9197-96eb38cfa09f", Email = "marko.tomic@gmail.com", EmailConfirmed = true, LockoutEnabled = true, LockoutEnd = null, NormalizedEmail = "marko.tomic@gmail.com", NormalizedUserName = "MARKO.TOMIC", PasswordHash = "AQAAAAEAACcQAAAAEAN/cpXD+xr3Me/MXjLLnjF0WVeahg2LXpQA60LJZmEoNyhsiQJ9W6YIa37K8xc8Gg==", PhoneNumberConfirmed = false, SecurityStamp = "UA7WPKZZXP7PZHWOECONNM3TSSTLIYYC", TwoFactorEnabled = false, UserName = "marko.tomic" };
            PetOwner p2 = new PetOwner() { Id = "3", RegistrationDate = DateTime.Now, FirstName = "Petar", LastName = "Jovanović", PhoneNumber = "063293734", StreetAdress = "Jove Ilića 152", Municipality = "Voždovac", AccessFailedCount = 0, ConcurrencyStamp = "28861696-11f7-41f1-9197-96eb38cfa09f", Email = "petar.jovanovic@gmail.com", EmailConfirmed = true, LockoutEnabled = true, LockoutEnd = null, NormalizedEmail = "petar.jovanovic@gmail.com", NormalizedUserName = "PETAR.JOVANOVIC", PasswordHash = "AQAAAAEAACcQAAAAEAN/cpXD+xr3Me/MXjLLnjF0WVeahg2LXpQA60LJZmEoNyhsiQJ9W6YIa37K8xc8Gg==", PhoneNumberConfirmed = false, SecurityStamp = "UA7WPKZZXP7PZHWOECONNM3TSSTLIYYC", TwoFactorEnabled = false, UserName = "petar.jovanovic" };
            PetOwner p3 = new PetOwner() { Id = "4", RegistrationDate = DateTime.Now, FirstName = "Milica", LastName = " Nikolić", PhoneNumber = "0627189273", StreetAdress = "Vojvode Stepe 371", Municipality = "Voždovac", AccessFailedCount = 0, ConcurrencyStamp = "28861696-11f7-41f1-9197-96eb38cfa09f", Email = "milica.nikolic@gmail.com", EmailConfirmed = true, LockoutEnabled = true, LockoutEnd = null, NormalizedEmail = "milica.nikolic@gmail.com", NormalizedUserName = "MILICA.NIKOLIC", PasswordHash = "AQAAAAEAACcQAAAAEAN/cpXD+xr3Me/MXjLLnjF0WVeahg2LXpQA60LJZmEoNyhsiQJ9W6YIa37K8xc8Gg==", PhoneNumberConfirmed = false, SecurityStamp = "UA7WPKZZXP7PZHWOECONNM3TSSTLIYYC", TwoFactorEnabled = false, UserName = "milica.nikolic" };
            PetOwner p4 = new PetOwner() { Id = "5", RegistrationDate = DateTime.Now, FirstName = "Marija", LastName = "Tanasković", PhoneNumber = "0659384950", StreetAdress = "Paunova 14", Municipality = "Voždovac", AccessFailedCount = 0, ConcurrencyStamp = "28861696-11f7-41f1-9197-96eb38cfa09f", Email = "marija.tanaskovic@gmail.com", EmailConfirmed = true, LockoutEnabled = true, LockoutEnd = null, NormalizedEmail = "marija.tanaskovic@gmail.com", NormalizedUserName = "MARIJA.TANASKOVIC", PasswordHash = "AQAAAAEAACcQAAAAEAN/cpXD+xr3Me/MXjLLnjF0WVeahg2LXpQA60LJZmEoNyhsiQJ9W6YIa37K8xc8Gg==", PhoneNumberConfirmed = false, SecurityStamp = "UA7WPKZZXP7PZHWOECONNM3TSSTLIYYC", TwoFactorEnabled = false, UserName = "marija.tanaskovic" };
            PetOwner p5 = new PetOwner() { Id = "6", RegistrationDate = DateTime.Now, FirstName = "Tamara", LastName = "Stamenković", PhoneNumber = "0668392019", StreetAdress = "Peke Pavlovića 38", Municipality = "Voždovac", AccessFailedCount = 0, ConcurrencyStamp = "28861696-11f7-41f1-9197-96eb38cfa09f", Email = "tamara.stamenkovic@gmail.com", EmailConfirmed = true, LockoutEnabled = true, LockoutEnd = null, NormalizedEmail = "tamara.stamenkovic@gmail.com", NormalizedUserName = "TAMARA.STAMENKOVIC", PasswordHash = "AQAAAAEAACcQAAAAEAN/cpXD+xr3Me/MXjLLnjF0WVeahg2LXpQA60LJZmEoNyhsiQJ9W6YIa37K8xc8Gg==", PhoneNumberConfirmed = false, SecurityStamp = "UA7WPKZZXP7PZHWOECONNM3TSSTLIYYC", TwoFactorEnabled = false, UserName = "tamara.stamenkovic" };
            PetOwner p6 = new PetOwner() { Id = "7", RegistrationDate = DateTime.Now, FirstName = "Nikola", LastName = "Delibašić", PhoneNumber = "0607948213", StreetAdress = "Darvinova 29", Municipality = "Voždovac", AccessFailedCount = 0, ConcurrencyStamp = "28861696-11f7-41f1-9197-96eb38cfa09f", Email = "nikola.delibasic@gmail.com", EmailConfirmed = true, LockoutEnabled = true, LockoutEnd = null, NormalizedEmail = "nikola.delibasic@gmail.com", NormalizedUserName = "NIKOLA.DELIBASIC", PasswordHash = "AQAAAAEAACcQAAAAEAN/cpXD+xr3Me/MXjLLnjF0WVeahg2LXpQA60LJZmEoNyhsiQJ9W6YIa37K8xc8Gg==", PhoneNumberConfirmed = false, SecurityStamp = "UA7WPKZZXP7PZHWOECONNM3TSSTLIYYC", TwoFactorEnabled = false, UserName = "nikola.delibasic" };
            PetOwner p7 = new PetOwner() { Id = "8", RegistrationDate = DateTime.Now, FirstName = "Tomislav", LastName = "Manić", PhoneNumber = "0602829271", StreetAdress = "Bulevar Oslobođenja 257", Municipality = "Voždovac", AccessFailedCount = 0, ConcurrencyStamp = "28861696-11f7-41f1-9197-96eb38cfa09f", Email = "tomislav.manic@gmail.com", EmailConfirmed = true, LockoutEnabled = true, LockoutEnd = null, NormalizedEmail = "tomislav.manic@gmail.com", NormalizedUserName = "TOMISLAV.MANIC", PasswordHash = "AQAAAAEAACcQAAAAEAN/cpXD+xr3Me/MXjLLnjF0WVeahg2LXpQA60LJZmEoNyhsiQJ9W6YIa37K8xc8Gg==", PhoneNumberConfirmed = false, SecurityStamp = "UA7WPKZZXP7PZHWOECONNM3TSSTLIYYC", TwoFactorEnabled = false, UserName = "tomislav.manic" };

            context.Users.Add(admin);
            context.Users.Add(p1);
            context.Users.Add(p2);
            context.Users.Add(p3);
            context.Users.Add(p4);
            context.Users.Add(p5);
            context.Users.Add(p6);
            context.Users.Add(p7);
            
            context.SaveChanges();            
        }

        private static void seedPetTypesTable(VetMedicAppDbContext context)
        {
            context.PetTypes.AddRange(
                new PetType() { TypeName = "Pas" },
                new PetType() { TypeName = "Mačka" },
                new PetType() { TypeName = "Glodar" },
                new PetType() { TypeName = "Ptica" },
                new PetType() { TypeName = "Reptil" }
                );

            context.SaveChanges();
        }

        private static void seedServicesTable(VetMedicAppDbContext context)
        {
            context.Services.AddRange(
                new Service() { Name = "Šišanje(osnovno)", ServiceType = ServiceType.Higijenska },
                new Service() { Name = "Šišanje(frizura)", ServiceType = ServiceType.Higijenska },
                new Service() { Name = "Raščešljavanje ućebane dlake", ServiceType = ServiceType.Higijenska },
                new Service() { Name = "Antiparazitski tretman", ServiceType = ServiceType.Higijenska },
                new Service() { Name = "Tretman redukcije linjanja", ServiceType = ServiceType.Higijenska },
                new Service() { Name = "Treman između šišanja", ServiceType = ServiceType.Higijenska },
                new Service() { Name = "Kupanje", ServiceType = ServiceType.Higijenska },
                new Service() { Name = "Sečenje noktiju", ServiceType = ServiceType.Higijenska },
                new Service() { Name = "Skraćivanje kljuna", ServiceType = ServiceType.Higijenska }
                );

            context.SaveChanges();
        }
    }
}

