﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace VetMedicApp.Data
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<VetMedicAppDbContext>
    {
        public VetMedicAppDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<VetMedicAppDbContext>();
            optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=VetMedicAppDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            VetMedicAppDbContext context = new VetMedicAppDbContext(optionsBuilder.Options);

            if (!context.AllMigrationsApplied())
            {
                context.Database.Migrate();
                context.EnsureDatabaseSeeded();
            }

            return context;
        }
    }
}
