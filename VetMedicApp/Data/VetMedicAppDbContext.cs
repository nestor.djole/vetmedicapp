﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using VetMedicApp.Models;

namespace VetMedicApp.Data
{
    public class VetMedicAppDbContext : IdentityDbContext<ApplicationUser>
    {
        public VetMedicAppDbContext(DbContextOptions<VetMedicAppDbContext> options) : base(options)
        { }

        public DbSet<Pet> Pets { get; set; }
        public DbSet<PetType> PetTypes { get; set; }
        public DbSet<PetOwner> PetOwners { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Admin> Admins { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            string connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=VetMedicAppDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            options.UseSqlServer(connectionString);
            //options.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Pet>().ToTable("Pets");
            modelBuilder.Entity<PetType>().ToTable("PetTypes");
            modelBuilder.Entity<PetOwner>().ToTable("PetOwners");
            modelBuilder.Entity<Service>().ToTable("Services");
            modelBuilder.Entity<Event>().ToTable("Events");
            modelBuilder.Entity<Admin>().ToTable("Admins");

            #region PetTableConstraint

            modelBuilder.Entity<Pet>()
                .Property(p => p.PetId)
                .IsRequired();

            modelBuilder.Entity<Pet>()
                .Property(p => p.MicrochipNumber)
                .IsRequired();

            modelBuilder.Entity<Pet>()
                .Property(p => p.Name)
                .IsRequired();

            modelBuilder.Entity<Pet>()
                .HasOne(p => p.PetType)
                .WithMany(pt => pt.Pets);

            modelBuilder.Entity<Pet>()
                .HasOne(p => p.Owner)
                .WithMany(o => o.Pets);

            modelBuilder.Entity<Pet>()
                .Property(p => p.Race)
                .IsRequired();

            modelBuilder.Entity<Pet>()
                .Property(p => p.Gender)
                .IsRequired();

            modelBuilder.Entity<Pet>()
                .Property(p => p.PassportNumber)
                .IsRequired();

            modelBuilder.Entity<Pet>()
                .Property(p => p.Color)
                .IsRequired();

            modelBuilder.Entity<Pet>()
               .HasMany(p => p.PetServices)
               .WithOne(e => e.Pet)
               .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region PetTypeTableConstraint

            modelBuilder.Entity<PetType>()
                .Property(p => p.PetTypeId)
                .IsRequired();

            modelBuilder.Entity<PetType>()
                .Property(p => p.TypeName)
                .IsRequired();

            modelBuilder.Entity<PetType>()
                .HasMany(p => p.Pets)
                .WithOne(p => p.PetType)
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region PetOwnerTableConstraint

            modelBuilder.Entity<PetOwner>()
                .Property(p => p.FirstName)
                .IsRequired();

            modelBuilder.Entity<PetOwner>()
                .Property(p => p.LastName)
                .IsRequired();

            modelBuilder.Entity<PetOwner>()
               .Property(p => p.UserName)
               .IsRequired();

            modelBuilder.Entity<PetOwner>()
                .Property(p => p.PhoneNumber)
                .IsRequired();

            modelBuilder.Entity<PetOwner>()
                .Property(p => p.Email)
                .IsRequired();

            modelBuilder.Entity<PetOwner>()
                .Property(p => p.StreetAdress)
                .IsRequired();

            modelBuilder.Entity<PetOwner>()
                .Property(p => p.Municipality)
                .IsRequired();

            modelBuilder.Entity<PetOwner>()
                .HasMany(p => p.Pets)
                .WithOne(p => p.Owner)
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region ServiceTableConstraint

            modelBuilder.Entity<Service>()
                .Property(s => s.ServiceId)
                .IsRequired();

            modelBuilder.Entity<Service>()
                .Property(s => s.Name)
                .IsRequired();

            modelBuilder.Entity<Service>()
                .Property(s => s.ServiceType)
                .IsRequired();

            modelBuilder.Entity<Service>()
               .HasMany(p => p.PetServices)
               .WithOne(e => e.Service)
               .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region PetServiceTableConstraint

            modelBuilder.Entity<PetService>()
                .HasKey(ps => new { ps.PetId, ps.ServiceId });

            modelBuilder.Entity<PetService>()
                .HasMany(ps => ps.Events)
                .WithOne(e => e.PetService)
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region EventTableConstraint

            modelBuilder.Entity<Event>()
                .Property(e => e.EventId)
                .IsRequired();

            modelBuilder.Entity<Event>()
                .Property(e => e.EventDateTime)
                .IsRequired();

            modelBuilder.Entity<Event>()
                .Property(e => e.PetServiceId)
                .IsRequired();

            modelBuilder.Entity<Event>()
                .HasOne(e => e.PetService)
                .WithMany(ps => ps.Events);

            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }

}