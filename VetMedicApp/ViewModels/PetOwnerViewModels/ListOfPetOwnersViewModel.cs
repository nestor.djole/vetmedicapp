﻿using VetMedicApp.Models;
using System.Collections.Generic;

namespace VetMedicApp.ViewModels.PetOwnerViewModels
{
    public class ListOfPetOwnersViewModel
    {
        public ListOfPetOwnersViewModel()
        {
            PetOwners = new List<PetOwner>();
        }

        public List<PetOwner> PetOwners { get; set; }
    }
}
