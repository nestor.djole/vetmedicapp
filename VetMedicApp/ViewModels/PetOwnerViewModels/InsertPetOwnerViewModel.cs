﻿using System.ComponentModel.DataAnnotations;

namespace VetMedicApp.ViewModels.PetOwnerViewModels
{
    public class InsertPetOwnerViewModel
    {
        [Required(ErrorMessage = "*(Morate uneti ime)")]
        [Display(Name = "Ime")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "*(Ime mora da ima između 3 i 20 karaktera)")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "*(Morate uneti prezime)")]
        [Display(Name = "Prezime")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "*(Prezime mora da ima između 3 i 20 karaktera)")]
        public string LastName { get; set; }

        [Display(Name = "Ulica i broj")]
        public string StreetAdress { get; set; }

        [Display(Name = "Opština")]
        public string Municipality { get; set; }

        [Required(ErrorMessage = "*(Morate uneti broj telefona)")]
        [Display(Name = "Broj telefona")]
        [StringLength(20, MinimumLength = 8, ErrorMessage = "*(Broj telefona mora imate minimum 8 cifara)")]
        public string PhoneNumber { get; set; }

        [EmailAddress(ErrorMessage = "*(Unesite ispravnu e-adresu)")]
        [Display(Name = "E-adresa")]
        public string Email { get; set; }

        [Required(ErrorMessage = "*(Morate uneti korisničko ime)")]
        [Display(Name = "Korisničko ime")]
        [StringLength(20, MinimumLength = 7, ErrorMessage = "*(Korisničko ime mora da ima između 7 i 20 karaktera)")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "*(Morate uneti šifru)")]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "*(Šifra treba da ima između 6 i 100 karaktera)")]
        [DataType(DataType.Password)]
        [Display(Name = "Šifra")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "*(Morate potvrditi šifru)")]
        [Compare("Password", ErrorMessage = "*(Šifre se ne poklapaju)")]
        public string ConfirmPassword { get; set; }
    }
}
