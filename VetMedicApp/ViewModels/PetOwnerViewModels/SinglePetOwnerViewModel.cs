﻿using System.Drawing;
using VetMedicApp.Models;
namespace VetMedicApp.ViewModels.PetOwnerViewModels
{
    public class SinglePetOwnerViewModel
    {
        public PetOwner PetOwner { get; set; }

        public byte[] QRCode { get; set; }
    }
}
