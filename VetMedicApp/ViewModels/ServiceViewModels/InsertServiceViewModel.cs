﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VetMedicApp.Models;

namespace VetMedicApp.ViewModels.ServiceViewModels
{
    public class InsertServiceViewModel
    {
        public List<ServiceType> AllServiceTypes { get; set; } = new List<ServiceType>();

        [Required(ErrorMessage = "*(Morate uneti naziv)")]
        [Display(Name = "Naziv:")]
        [StringLength(100, MinimumLength = 5, ErrorMessage = "*(Naziv mora da ima između 5 i 100 karaktera)")]
        public string Name { get; set; }

        [Required(ErrorMessage = "*(Morate izabrati tip)")]
        [Display(Name = "Tip")]
        public ServiceType ServiceType { get; set; }
    }
}
