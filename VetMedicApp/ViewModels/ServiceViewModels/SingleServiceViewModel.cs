﻿using VetMedicApp.Models;

namespace VetMedicApp.ViewModels.ServiceViewModels
{
    public class SingleServiceViewModel
    {
        public Service Service { get; set; }
    }
}
