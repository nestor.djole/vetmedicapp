﻿using System.Collections.Generic;
using VetMedicApp.Models;

namespace VetMedicApp.ViewModels.ServiceViewModels
{
    public class ListOfServicesViewModel
    {
        public ListOfServicesViewModel()
        {
            Services = new List<Service>();
        }

        public List<Service> Services { get; set; }
    }
}
