﻿using VetMedicApp.Models;

namespace VetMedicApp.ViewModels.PetViewModels
{
    public class SinglePetViewModel
    {
        public Pet Pet { get; set; }
    }
}
