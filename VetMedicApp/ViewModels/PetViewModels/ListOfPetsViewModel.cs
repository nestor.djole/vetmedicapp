﻿using System.Collections.Generic;
using VetMedicApp.Models;

namespace VetMedicApp.ViewModels.PetViewModels
{
    public class ListOfPetsViewModel
    {
        public ListOfPetsViewModel()
        {
            Pets = new List<Pet>();
        }

        public List<Pet> Pets { get; set; }
    }
}
