﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VetMedicApp.Models;

namespace VetMedicApp.ViewModels.PetViewModels
{
    public class UpdatePetViewModel
    {
        public List<PetType> AllPetTypes { get; set; } = new List<PetType>();
        public List<Gender> AllGenders { get; set; } = new List<Gender>();

        public long PetId { get; set; }
        public Gender Gender { get; set; }
        public PetOwner Owner { get; set; }
        public PetType PetType { get; set; }

        [Required(ErrorMessage = "*(Morate uneti ime)")]
        [Display(Name = "Ime")]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Ime treba da ima između 2 i 20 karaktera")]
        public string Name { get; set; }

        [Required(ErrorMessage = "*(Morate uneti rasu)")]
        [Display(Name = "Rasa")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Rasa treba da ima između 3 i 50 karaktera")]
        public string Race { get; set; }

        [Required(ErrorMessage = "*(Morate uneti boju dlake)")]
        [Display(Name = "Boja dlake")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Boja dlake treba da ima između 3 i 50 karaktera")]
        public string Color { get; set; }

        [Required(ErrorMessage = "Unesite broj mikročipa")]
        [Display(Name = "Broj mikročipa")]
        public string MicrochipNumber { get; set; }

        [Display(Name = "Broj pasoša")]
        //TODO:
        [StringLength(20, MinimumLength = 8, ErrorMessage = "Broj pasoša nije ispravan")]
        public string PassportNumber { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Datum rođenja")]
        public DateTime? DateOfBirth { get; set; }
    }
}
