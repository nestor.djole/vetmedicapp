﻿using VetMedicApp.Models;

namespace VetMedicApp.ViewModels.EventViewModels
{
    public class SingleEventViewModel
    {
        public Event Event { get; set; }
    }
}
