﻿using System.Collections.Generic;
using VetMedicApp.Models;

namespace VetMedicApp.ViewModels.EventViewModels
{
    public class ListOfEventsViewModel
    {       
        public List<Event> Events { get; set; } = new List<Event>();
    }
}
