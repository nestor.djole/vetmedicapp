﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VetMedicApp.Models;

namespace VetMedicApp.ViewModels.EventViewModels
{
    public class InsertEventViewModel
    {
        public List<Service> AllServices { get; set; } = new List<Service>();
        public List<Pet> AllPets { get; set; } = new List<Pet>();
        [Required(ErrorMessage = "*(Morate izabrati uslugu)")]
        [Display(Name = "Usluga")]
        public long ServiceId { get; set; }
        [Required(ErrorMessage = "*(Morate izabrati ljubimca)")]
        [Display(Name = "Ljubimac")]
        public long PetId { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        [Display(Name = "Datum")]
        public DateTime Date { get; set; }
        [Required(ErrorMessage= "*(Morate uneti vreme zakazivanja)")]
        [Range(6, 22, ErrorMessage = "Uneta vrednost za sate mora biti broj između 6 i 22")]
        public int Hours { get; set; }
        [Required(ErrorMessage = "*(Morate uneti vreme zakazivanja)")]        
        [Range(0, 59, ErrorMessage = "Uneta vrednost za minute mora biti broj između 0 i 59")]
        public int Minutes { get; set; }
    }
}
