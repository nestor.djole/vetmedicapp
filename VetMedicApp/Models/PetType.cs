﻿using System.Collections.Generic;

namespace VetMedicApp.Models
{
    public class PetType
    {
        public PetType()
        {
            Pets = new List<Pet>();
        }
        public long PetTypeId { get; set; }
        public string TypeName { get; set; }
        public List<Pet> Pets { get; set; }
    }
}