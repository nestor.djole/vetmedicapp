﻿using System;

namespace VetMedicApp.Models
{
    public class Event
    {
        public long EventId { get; set; }
        public DateTime EventDateTime { get; set; }
        public long PetServiceId { get; set; }
        public PetService PetService { get; set; }
    }
}
