﻿using System;
using System.Collections.Generic;

namespace VetMedicApp.Models
{
    public enum Gender
    {
        Muški,
        Ženski
    }

    public class Pet
    {
        public long PetId { get; set; }
        public string Name { get; set; }
        public string Race { get; set; }
        public string Color { get; set; }
        public Gender Gender { get; set; }
        public string MicrochipNumber { get; set; }
        public string PassportNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public PetOwner Owner { get; set; }
        public PetType PetType { get; set; }
        public List<PetService> PetServices { get; set; } = new List<PetService>();
    }
}
