﻿using System.Collections.Generic;

namespace VetMedicApp.Models
{
    public class PetService
    {
        public PetService()
        {
            Events = new List<Event>();
        }

        public long PetId { get; set; }
        public Pet Pet { get; set; }
        public long ServiceId { get; set; }
        public Service Service { get; set; }
        public List<Event> Events { get; set; }
    }
}
