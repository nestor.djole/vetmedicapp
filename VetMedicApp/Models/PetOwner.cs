﻿using System;
using System.Collections.Generic;

namespace VetMedicApp.Models
{
    public class PetOwner : ApplicationUser
    {
        public PetOwner()
        {
            Pets = new List<Pet>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StreetAdress { get; set; }
        public string Municipality { get; set; }
        public DateTime RegistrationDate { get; set; }
        public List<Pet> Pets { get; set; }
    }
}
