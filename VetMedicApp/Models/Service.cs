﻿using System.Collections.Generic;

namespace VetMedicApp.Models
{
    public enum ServiceType
    {
        Higijenska,
        Medicinska
    }

    public class Service
    {
        public Service()
        {
            PetServices = new List<PetService>();
        }

        public long ServiceId { get; set; }
        public string Name { get; set; }
        public ServiceType ServiceType { get; set; }
        public List<PetService> PetServices { get; set; }
    }
}
