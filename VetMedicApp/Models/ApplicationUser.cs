﻿using Microsoft.AspNetCore.Identity;

namespace VetMedicApp.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
