﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using VetMedicApp.Data;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public class EventLogic : IEventLogic
    {
        private readonly VetMedicAppDbContext _context;

        public EventLogic(VetMedicAppDbContext context)
        {
            _context = context;
        }

        public Event Delete(long id)
        {
            throw new NotImplementedException();
        }

        public List<Event> GetAll()
        {
            try
            {
                return _context.Events
                    .Include(e => e.PetService)
                    .ThenInclude(ps => ps.Pet)
                    .Include(e => e.PetService)
                    .ThenInclude(ps => ps.Service)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Nisu vraćeni sva zakazivanja! Greška: {ex.Message}");
            }
        }

        public Event GetById(long id)
        {
            throw new NotImplementedException();
        }

        public Event Insert(Event ev)
        {
            try
            {
                if (ev != null)
                {
                    _context.Events.Add(ev);
                    _context.SaveChanges();

                    return ev;
                }
                throw new Exception($"Prosleđeni zakazani tretman nema vrednost.");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom zakazivanja! Greška: {ex.Message}");
            }
        }
    }
}
