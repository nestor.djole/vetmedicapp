﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using VetMedicApp.Data;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public class PetOwnerLogic : IPetOwnerLogic
    {
        private readonly VetMedicAppDbContext _context;

        public PetOwnerLogic(VetMedicAppDbContext context)
        {
            _context = context;
        }

        public PetOwner Delete(string username)
        {
            try
            {
                PetOwner petOwner = GetByUserName(username);

                if (petOwner != null)
                {
                    _context.Remove(petOwner);
                    _context.SaveChanges();
                    return petOwner;
                }
                throw new Exception("Klijent koga želite da izbrišete ne postoji!");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom brisanja klijenta! Greška: {ex.Message}");
            }
        }

        public List<PetOwner> GetAll()
        {
            try
            {
                return _context.PetOwners
                    .Include(p => p.Pets)
                    .ThenInclude(p => p.PetType)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom vraćanja svih klijenata! Greška: {ex.Message}");
            }
        }

        public PetOwner GetByUserName(string username)
        {
            try
            {
                return _context.PetOwners
                    .Include(p => p.Pets)
                    .SingleOrDefault(p => p.UserName.Equals(username));
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom pretrage klijenata! Greška: {ex.Message}");
            }
        }

        public PetOwner Insert(PetOwner petOwner)
        {
            try
            {
                if (petOwner != null)
                {
                    IdentityRole userRole = _context.Roles.SingleOrDefault(r => r.Name.ToLower().Equals("petowner"));

                    _context.UserRoles.Add(new IdentityUserRole<string>() { RoleId = userRole.Id, UserId = petOwner.Id });
                    _context.SaveChanges();

                    return petOwner;
                }
                throw new Exception($"Prosleđeni klijent nema vrednost.");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom unosa novog klijenta! Greška: {ex.Message}");
            }
        }

        public PetOwner Update(string username, PetOwner user)
        {
            try
            {
                PetOwner petOwner = GetByUserName(username);

                if (petOwner != null)
                {
                    petOwner.FirstName = user.FirstName;
                    petOwner.LastName = user.LastName;
                    petOwner.PhoneNumber = user.PhoneNumber;
                    petOwner.Email = user.Email;
                    petOwner.StreetAdress = user.StreetAdress;
                    petOwner.Municipality = user.Municipality;

                    _context.PetOwners.Update(petOwner);
                    _context.SaveChanges();

                    return petOwner;
                }
                throw new Exception("Klijent koga želite da ažurirate ne postoji!");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom ažuriranja klijenta! Greška: {ex.Message}");
            }
        }
    }
}
