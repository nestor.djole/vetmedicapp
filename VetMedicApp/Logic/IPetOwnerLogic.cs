﻿using System.Collections.Generic;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public interface IPetOwnerLogic
    {
        List<PetOwner> GetAll();

        PetOwner GetByUserName(string username);

        PetOwner Insert(PetOwner petOwner);

        PetOwner Update(string username, PetOwner user);

        PetOwner Delete(string username);
    }
}
