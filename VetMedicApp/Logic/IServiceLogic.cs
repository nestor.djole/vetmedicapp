﻿using System.Collections.Generic;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public interface IServiceLogic
    {
        List<Service> GetAll();

        Service GetById(long id);

        Service Insert(Service petOwner);

        Service Update(long id);

        Service Delete(long id);
    }
}
