﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VetMedicApp.Data;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public class PetTypeLogic : IPetTypeLogic
    {
        private readonly VetMedicAppDbContext _context;

        public PetTypeLogic(VetMedicAppDbContext context)
        {
            _context = context;
        }

        public List<PetType> GetAll()
        {
            try
            {
                return _context.PetTypes.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom vraćanja svih vrsta ljubimaca! Greška: {ex.Message}");
            }

        }

        public PetType GetById(long id)
        {
            try
            {
                return _context.PetTypes.SingleOrDefault(p => p.PetTypeId == id);
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom vraćanja vrste ljubimca! Greška: {ex.Message}");
            }
        }
    }
}
