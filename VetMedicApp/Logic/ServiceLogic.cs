﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VetMedicApp.Data;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public class ServiceLogic : IServiceLogic
    {
        private readonly VetMedicAppDbContext _context;

        public ServiceLogic(VetMedicAppDbContext context)
        {
            _context = context;
        }

        public Service Delete(long id)
        {
            try
            {
                Service service = GetById(id);

                if (service != null)
                {
                    _context.Remove(service);
                    _context.SaveChanges();
                    return service;
                }
                throw new Exception("Usluga koju želite da izbrišete ne postoji!");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom brisanja usluge! Greška: {ex.Message}");
            }
        }

        public List<Service> GetAll()
        {
            try
            {
                return _context.Services
                    .ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Nisu vraćene sve usluge! Greška: {ex.Message}");
            }
        }

        public Service GetById(long id)
        {
            try
            {
                return _context.Services
                    .FirstOrDefault(s => s.ServiceId == id);
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom traženja usluge! Greška: {ex.Message}");
            }

        }

        public Service Insert(Service service)
        {
            try
            {
                if (service != null)
                {
                    _context.Services.Add(service);
                    _context.SaveChanges();

                    return service;
                }
                throw new Exception($"Prosleđena usluga nema vrednost.");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom unosa nove usluge! Greška: {ex.Message}");
            }
        }

        public Service Update(long id)
        {
            try
            {
                Service service = GetById(id);

                if (service != null)
                {
                    service.Name = service.Name;
                    service.ServiceType = service.ServiceType;

                    _context.Services.Update(service);
                    _context.SaveChanges();

                    return service;
                }
                throw new Exception("Usluga koga želite da ažurirate ne postoji u bazi!");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom ažuriranja usluge! Greška: {ex.Message}");
            }
        }
    }
}
