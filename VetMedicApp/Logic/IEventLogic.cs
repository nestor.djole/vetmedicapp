﻿using System.Collections.Generic;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public interface IEventLogic
    {
        List<Event> GetAll();

        Event GetById(long id);

        Event Insert(Event ev);        

        Event Delete(long id);
    }
}
