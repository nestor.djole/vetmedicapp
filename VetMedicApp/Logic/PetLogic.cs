﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using VetMedicApp.Data;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public class PetLogic : IPetLogic
    {
        private readonly VetMedicAppDbContext _context;

        public PetLogic(VetMedicAppDbContext context)
        {
            _context = context;
        }

        public Pet Delete(long id)
        {
            try
            {
                Pet pet = GetById(id);

                if (pet != null)
                {
                    _context.Remove(pet);
                    _context.SaveChanges();
                    return pet;
                }
                throw new Exception("Ljubimac koga želite da izbrišete ne postoji!");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom brisanja ljubimca! Greška: {ex.Message}");
            }
        }

        public List<Pet> GetAll()
        {
            try
            {
                return _context.Pets
                    .Include(p => p.Owner)
                    .Include(p => p.PetType)
                    .Include(p => p.PetServices)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Nisu vraćeni svi ljubimci! Greška: {ex.Message}");
            }
        }

        public Pet GetById(long id)
        {
            try
            {
                return _context.Pets
                    .Include(p => p.Owner)
                    .Include(p => p.PetType)
                    .Include(p => p.PetServices)
                    .FirstOrDefault(p => p.PetId == id);
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom traženja ljubimca! Greška: {ex.Message}");
            }
        }

        public Pet Insert(Pet pet)
        {
            try
            {
                if (pet != null)
                {
                    _context.Pets.Add(pet);
                    _context.SaveChanges();

                    return pet;
                }
                throw new Exception($"Prosleđeni ljubimac nema vrednost.");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom unosa novog ljubimca! Greška: {ex.Message}");
            }
        }

        public Pet Update(long id, Pet p)
        {
            try
            {
                Pet pet = GetById(id);

                if (pet != null)
                {
                    pet.Name = p.Name;
                    pet.Color = p.Color;
                    pet.Race = p.Race;
                    pet.DateOfBirth = p.DateOfBirth;
                    pet.PassportNumber = p.PassportNumber;
                    pet.PetType = p.PetType;
                    pet.Gender = p.Gender;
                    pet.MicrochipNumber = p.MicrochipNumber;

                    _context.Pets.Update(pet);
                    _context.SaveChanges();

                    return pet;
                }
                throw new Exception("Ljubimac koga želite da ažurirate ne postoji u bazi!");
            }
            catch (Exception ex)
            {
                throw new Exception($"Greška prilikom ažuriranja ljubimca! Greška: {ex.Message}");
            }
        }
    }
}
