﻿using System.Collections.Generic;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public interface IPetTypeLogic
    {
        List<PetType> GetAll();

        PetType GetById(long id);
    }
}
