﻿using System.Collections.Generic;
using VetMedicApp.Models;

namespace VetMedicApp.Logic
{
    public interface IPetLogic
    {
        List<Pet> GetAll();

        Pet GetById(long id);

        Pet Insert(Pet pet);

        Pet Update(long id, Pet p);

        Pet Delete(long id);
    }
}
