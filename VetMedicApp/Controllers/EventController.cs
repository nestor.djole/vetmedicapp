﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using VetMedicApp.Data;
using VetMedicApp.Logic;
using VetMedicApp.Models;
using VetMedicApp.ViewModels;
using VetMedicApp.ViewModels.EventViewModels;

namespace VetMedicApp.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("[controller]/[action]")]
    public class EventController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly VetMedicAppDbContext _context;

        public EventController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            VetMedicAppDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [HttpGet]
        public IActionResult Planner()
        {
            IEventLogic logic = new EventLogic(_context);
            ListOfEventsViewModel model = new ListOfEventsViewModel()
            {
                Events = logic.GetAll()
            };
            return View(model);
        }

        [HttpGet]
        public IActionResult Insert()
        {
            try
            {
                InsertEventViewModel evm = new InsertEventViewModel();
                IServiceLogic serviceLogic = new ServiceLogic(_context);
                IPetLogic petLogic = new PetLogic(_context);
                
                evm.AllServices = serviceLogic.GetAll();
                evm.AllPets = petLogic.GetAll();

                return View(evm);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Insert(InsertEventViewModel model)
        {
            try
            {
                IServiceLogic serviceLogic = new ServiceLogic(_context);
                IPetLogic petLogic = new PetLogic(_context);

                if (ModelState.IsValid)
                {

                    Pet pet = petLogic.GetById(model.PetId) ?? throw new Exception("Ljubimac koga ste izabrali ne postoji u bazi!");
                    Service service= serviceLogic.GetById(model.ServiceId) ?? throw new Exception("Usluga koju ste izabrali ne postoji u bazi!");
                    
                    var ev = new Event()
                    {
                        PetService = new PetService()
                        {
                            PetId = pet.PetId,
                            Pet = pet,
                            ServiceId = service.ServiceId,
                            Service = service
                        },
                        EventDateTime = new DateTime(model.Date.Year, model.Date.Month, model.Date.Day, model.Hours, model.Minutes, 0)                        
                    };

                    IEventLogic logic = new EventLogic(_context);
                    Event insertedEvent = logic.Insert(ev);

                    return View("InsertSuccessful", new SingleEventViewModel() { Event = insertedEvent});
                }
                
                model.AllServices = serviceLogic.GetAll();
                model.AllPets = petLogic.GetAll();
                return View(model);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }
    }
}
