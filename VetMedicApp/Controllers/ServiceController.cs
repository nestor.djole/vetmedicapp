﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using VetMedicApp.Data;
using VetMedicApp.Logic;
using VetMedicApp.Models;
using VetMedicApp.ViewModels;
using VetMedicApp.ViewModels.ServiceViewModels;

namespace VetMedicApp.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("[controller]/[action]")]
    public class ServiceController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly VetMedicAppDbContext _context;

        public ServiceController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            VetMedicAppDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [HttpGet]
        public IActionResult Overview()
        {
            try
            {
                ListOfServicesViewModel svm = new ListOfServicesViewModel();
                IServiceLogic serviceLogic = new ServiceLogic(_context);

                svm.Services = serviceLogic.GetAll();

                return View(svm);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult Insert()
        {
            try
            {
                InsertServiceViewModel svm = new InsertServiceViewModel();
                IServiceLogic serviceLogic = new ServiceLogic(_context);

                svm.AllServiceTypes.Add(ServiceType.Higijenska);
                svm.AllServiceTypes.Add(ServiceType.Medicinska);

                return View(svm);


            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Insert(InsertServiceViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var service = new Service()
                    {
                        Name = model.Name,
                        ServiceType = model.ServiceType
                    };

                    IServiceLogic serviceLogic = new ServiceLogic(_context);
                    Service insertedService = serviceLogic.Insert(service);

                    return View("InsertSuccessful", new SingleServiceViewModel() { Service = insertedService });
                }

                model.AllServiceTypes.Add(ServiceType.Higijenska);
                model.AllServiceTypes.Add(ServiceType.Medicinska);
                return View(model);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult Export()
        {
            try
            {

                byte[] fileContents;

                IServiceLogic logic = new ServiceLogic(_context);

                List<Service> services = logic.GetAll();

                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Usluge");

                    worksheet.Cells[1, 1].Value = "Naziv";
                    worksheet.Cells[1, 2].Value = "Tip";

                    for (int i = 1; i <= services.Count; i++)
                    {
                        worksheet.Cells[i + 1, 1].Value = services.ElementAt(i - 1).Name;
                        worksheet.Cells[i + 1, 2].Value = services.ElementAt(i - 1).ServiceType;
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    worksheet.Row(1).Style.Font.Bold = true;

                    fileContents = package.GetAsByteArray();
                }

                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"Export-Usluge-{DateTime.Now.ToString("(dd.MM.yyyy)")}.xlsx"
                );
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }
    }
}