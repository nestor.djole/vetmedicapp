﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using VetMedicApp.Data;
using VetMedicApp.Logic;
using VetMedicApp.Models;
using VetMedicApp.ViewModels;
using VetMedicApp.ViewModels.PetViewModels;


namespace VetMedicApp.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("[controller]/[action]")]
    public class PetController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly VetMedicAppDbContext _context;

        public PetController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            VetMedicAppDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        [HttpGet]
        public IActionResult Overview()
        {
            try
            {
                ListOfPetsViewModel pvm = new ListOfPetsViewModel();
                IPetLogic petLogic = new PetLogic(_context);

                pvm.Pets = petLogic.GetAll();

                return View(pvm);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }

        }

        [HttpGet("{username}")]
        public IActionResult Insert(string username)
        {
            try
            {
                InsertPetViewModel pvm = new InsertPetViewModel();
                IPetOwnerLogic petOwnerLogic = new PetOwnerLogic(_context);
                IPetTypeLogic petTypeLogic = new PetTypeLogic(_context);

                pvm.Owner = petOwnerLogic.GetByUserName(username) ?? throw new Exception("Klijent kome dodeljujete ljubimca ne postoji!");
                pvm.AllPetTypes = petTypeLogic.GetAll();

                pvm.AllGenders.Add(Gender.Muški);
                pvm.AllGenders.Add(Gender.Ženski);

                return View(pvm);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpPost("{username}")]
        [ValidateAntiForgeryToken]
        public IActionResult Insert(InsertPetViewModel model, string username)
        {
            try
            {
                IPetOwnerLogic petOwnerLogic = new PetOwnerLogic(_context);
                IPetTypeLogic petTypeLogic = new PetTypeLogic(_context);

                if (ModelState.IsValid)
                {

                    PetOwner owner = petOwnerLogic.GetByUserName(username) ?? throw new Exception("Klijent kome dodeljujete ljubimca ne postoji!");
                    PetType petType = petTypeLogic.GetById(model.PetType.PetTypeId) ?? throw new Exception("Izabrana vrsta ljubimca ne postoji!");

                    var pet = new Pet()
                    {
                        Name = model.Name,
                        Race = model.Race,
                        Gender = model.Gender,
                        Color = model.Color ?? "",
                        Owner = owner,
                        PetType = petType,
                        PassportNumber = model.PassportNumber ?? "",
                        MicrochipNumber = model.MicrochipNumber,
                        DateOfBirth = model.DateOfBirth,
                    };

                    IPetLogic logic = new PetLogic(_context);
                    Pet insertedPet = logic.Insert(pet);

                    return View("InsertSuccessful", new SinglePetViewModel() { Pet = insertedPet });
                }

                model.Owner = petOwnerLogic.GetByUserName(username) ?? throw new Exception("Izabrani klijent ne postoji u bazi");
                model.AllPetTypes = petTypeLogic.GetAll();
                model.AllGenders.Add(Gender.Muški);
                model.AllGenders.Add(Gender.Ženski);
                return View(model);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpGet("{id}")]
        public IActionResult Update(long id)
        {
            try
            {
                IPetLogic petLogic = new PetLogic(_context);
                IPetTypeLogic petTypeLogic = new PetTypeLogic(_context);

                Pet p = petLogic.GetById(id) ?? throw new Exception($"Ljubimac koga želite da ažurirate ne postoji u bazi!");
                List<PetType> allPetTypes = petTypeLogic.GetAll();
                List<Gender> allGenders = new List<Gender>() { Gender.Muški, Gender.Ženski };

                UpdatePetViewModel pvm = new UpdatePetViewModel()
                {
                    Name = p.Name,
                    Color = p.Color,
                    Race = p.Race,
                    DateOfBirth = p.DateOfBirth,
                    PassportNumber = p.PassportNumber,
                    PetType = p.PetType,
                    Gender = p.Gender,
                    MicrochipNumber = p.MicrochipNumber,
                    Owner = p.Owner,
                    AllPetTypes = allPetTypes,
                    AllGenders = allGenders,
                    PetId = p.PetId
                };

                return View(pvm);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpPost("{id}")]
        [ValidateAntiForgeryToken]
        public IActionResult Update(UpdatePetViewModel model, long id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    IPetTypeLogic petTypeLogic = new PetTypeLogic(_context);

                    var pet = new Pet()
                    {
                        Name = model.Name,
                        Color = model.Color ?? "",
                        Race = model.Race,
                        DateOfBirth = model.DateOfBirth ?? DateTime.MinValue,
                        PassportNumber = model.PassportNumber ?? "",
                        PetType = petTypeLogic.GetById(model.PetType.PetTypeId),
                        Gender = model.Gender,
                        MicrochipNumber = model.MicrochipNumber,
                    };

                    IPetLogic logic = new PetLogic(_context);
                    Pet updatedPet = logic.Update(id, pet);

                    return View("UpdateSuccessful", new SinglePetViewModel() { Pet = updatedPet });
                }
                return View(model);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpGet("{id}")]
        public IActionResult Details(long id)
        {
            try
            {
                SinglePetViewModel pvm = new SinglePetViewModel();
                IPetLogic petLogic = new PetLogic(_context);

                pvm.Pet = petLogic.GetById(id) ?? throw new Exception($"Ljubimac čije detalje zahtevate ne postoji u bazi!");

                return View(pvm);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }
        
        [HttpGet("{id}")]
        public IActionResult Delete(long id)
        {
            try
            {
                IPetLogic petLogic = new PetLogic(_context);

                petLogic.Delete(id);

                return RedirectToAction("Overview");
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult Export()
        {
            try
            {
                byte[] fileContents;

                IPetLogic logic = new PetLogic(_context);

                List<Pet> pets = logic.GetAll();

                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Ljubimci");

                    worksheet.Cells[1, 1].Value = "Ime";
                    worksheet.Cells[1, 2].Value = "Vrsta";
                    worksheet.Cells[1, 3].Value = "Rasa";
                    worksheet.Cells[1, 4].Value = "Boja dlake";
                    worksheet.Cells[1, 5].Value = "Pol";
                    worksheet.Cells[1, 6].Value = "Datum rođenja";
                    worksheet.Cells[1, 7].Value = "Mikročip";
                    worksheet.Cells[1, 8].Value = "Датум микрочиповања";
                    worksheet.Cells[1, 9].Value = "Broj pasoša";
                    worksheet.Cells[1, 10].Value = "Датум издавања пасоша";
                    worksheet.Cells[1, 11].Value = "Vlasnik";

                    for (int i = 1; i <= pets.Count; i++)
                    {
                        worksheet.Cells[i + 1, 1].Value = pets.ElementAt(i - 1).Name;
                        worksheet.Cells[i + 1, 2].Value = pets.ElementAt(i - 1).PetType.TypeName;
                        worksheet.Cells[i + 1, 3].Value = pets.ElementAt(i - 1).Race;
                        worksheet.Cells[i + 1, 4].Value = pets.ElementAt(i - 1).Color;
                        worksheet.Cells[i + 1, 5].Value = pets.ElementAt(i - 1).Gender;
                        worksheet.Cells[i + 1, 6].Value = pets.ElementAt(i - 1).DateOfBirth.Equals(DateTime.MinValue) ? "" : pets.ElementAt(i - 1).DateOfBirth.ToString();
                        worksheet.Cells[i + 1, 7].Value = pets.ElementAt(i - 1).MicrochipNumber;
                        worksheet.Cells[i + 1, 9].Value = pets.ElementAt(i - 1).PassportNumber;

                        string petowner = $"{pets.ElementAt(i - 1).Owner.FirstName} {pets.ElementAt(i - 1).Owner.LastName} ({pets.ElementAt(i - 1).Owner.UserName})";
                        worksheet.Cells[i + 1, 11].Value = petowner;
                    }

                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    worksheet.Row(1).Style.Font.Bold = true;

                    fileContents = package.GetAsByteArray();
                }

                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"Export-Ljubimci-{DateTime.Now.ToString("(dd.MM.yyyy)")}.xlsx"
                );
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }
    }
}