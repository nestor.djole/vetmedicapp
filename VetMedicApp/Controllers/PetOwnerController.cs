﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using QRCoder;
using VetMedicApp.Data;
using VetMedicApp.Logic;
using VetMedicApp.Models;
using VetMedicApp.ViewModels;
using VetMedicApp.ViewModels.PetOwnerViewModels;

namespace VetMedicApp.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("[controller]/[action]")]
    public class PetOwnerController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly VetMedicAppDbContext _context;

        public PetOwnerController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger,
            VetMedicAppDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public IActionResult Insert()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View("Home/Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Insert(InsertPetOwnerViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = new PetOwner()
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        PhoneNumber = model.PhoneNumber,
                        Email = model.Email ?? "",
                        StreetAdress = model.StreetAdress ?? "",
                        Municipality = model.Municipality ?? "",
                        UserName = model.UserName,
                        RegistrationDate = DateTime.Now
                    };

                    var result = await _userManager.CreateAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        _logger.LogInformation("You have created a new account with role petowner.");

                        IPetOwnerLogic logic = new PetOwnerLogic(_context);
                        PetOwner insertedPetOwner = logic.Insert(user);

                        return View("InsertSuccessful", new SinglePetOwnerViewModel() { PetOwner = insertedPetOwner });
                    }
                    return View(model);
                }
                return View(model);
            }
            catch (Exception ex)
            {
                return View("Home/Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }            
        }

        [HttpGet]
        public IActionResult Overview()
        {
            try
            {
                ListOfPetOwnersViewModel petOwners = new ListOfPetOwnersViewModel();
                IPetOwnerLogic petOwnerLogic = new PetOwnerLogic(_context);

                petOwners.PetOwners = petOwnerLogic.GetAll();

                return View(petOwners);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }   
        }

        [HttpGet("{username}")]
        public IActionResult Details(string username)
        {
            try {
                SinglePetOwnerViewModel povm = new SinglePetOwnerViewModel();
                IPetOwnerLogic petOwnerLogic = new PetOwnerLogic(_context);

                povm.PetOwner = petOwnerLogic.GetByUserName(username) ?? throw new Exception($"Klijent sa korisničkim imenom \"{username}\" ne postoji");

                return View(povm);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpGet("{username}")]
        public IActionResult Update(string username)
        {
            try
            {
                IPetOwnerLogic petOwnerLogic = new PetOwnerLogic(_context);

                PetOwner po = petOwnerLogic.GetByUserName(username);

                UpdatePetOwnerViewModel povm = new UpdatePetOwnerViewModel()
                {
                    FirstName = po.FirstName,
                    LastName = po.LastName,
                    PhoneNumber = po.PhoneNumber,
                    Email = po.Email ?? "",
                    StreetAdress = po.StreetAdress ?? "",
                    Municipality = po.Municipality ?? "",
                    UserName = po.UserName,
                };

                return View(povm);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }

        [HttpPost("{username}")]
        [ValidateAntiForgeryToken]
        public IActionResult Update(UpdatePetOwnerViewModel model)
        {
            try {
                if (ModelState.IsValid)
                {
                    var user = new PetOwner()
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        PhoneNumber = model.PhoneNumber,
                        Email = model.Email ?? "",
                        StreetAdress = model.StreetAdress ?? "",
                        Municipality = model.Municipality ?? ""
                    };

                    _logger.LogInformation("You have modified account with role petowner.");

                    IPetOwnerLogic logic = new PetOwnerLogic(_context);
                    PetOwner updatedPetOwner = logic.Update(model.UserName, user);

                    return View("UpdateSuccessful", new SinglePetOwnerViewModel() { PetOwner = updatedPetOwner });
                }
                return View(model);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }
        
        [HttpGet("{username}")]
        public IActionResult Delete(string username)
        {
            try
            {
                IPetOwnerLogic petOwnerLogic = new PetOwnerLogic(_context);

                petOwnerLogic.Delete(username);

                return RedirectToAction("Overview");
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }            

        [HttpGet]
        public IActionResult Export()
        {
            try {
                byte[] fileContents;

                IPetOwnerLogic logic = new PetOwnerLogic(_context);

                List<PetOwner> petowners = logic.GetAll(); ;

                using (var package = new ExcelPackage())
                {
                    var worksheet = package.Workbook.Worksheets.Add("Klijenti");

                    worksheet.Cells[1, 1].Value = "Ime";
                    worksheet.Cells[1, 2].Value = "Prezime";
                    worksheet.Cells[1, 3].Value = "Korisničko ime";
                    worksheet.Cells[1, 4].Value = "Broj telefona";
                    worksheet.Cells[1, 5].Value = "Email";
                    worksheet.Cells[1, 6].Value = "Adresa";
                    worksheet.Cells[1, 7].Value = "Opština";
                    worksheet.Cells[1, 8].Value = "Datum registrovanja";
                    worksheet.Cells[1, 9].Value = "Ljubimci";

                    for (int i = 1; i <= petowners.Count; i++)
                    {
                        worksheet.Cells[i + 1, 1].Value = petowners.ElementAt(i - 1).FirstName;
                        worksheet.Cells[i + 1, 2].Value = petowners.ElementAt(i - 1).LastName;
                        worksheet.Cells[i + 1, 3].Value = petowners.ElementAt(i - 1).UserName;
                        worksheet.Cells[i + 1, 4].Value = petowners.ElementAt(i - 1).PhoneNumber;
                        worksheet.Cells[i + 1, 5].Value = petowners.ElementAt(i - 1).Email;
                        worksheet.Cells[i + 1, 6].Value = petowners.ElementAt(i - 1).StreetAdress;
                        worksheet.Cells[i + 1, 7].Value = petowners.ElementAt(i - 1).Municipality;
                        worksheet.Cells[i + 1, 8].Value = petowners.ElementAt(i - 1).RegistrationDate.ToShortDateString();
                        if (petowners.ElementAt(i - 1).Pets.Count > 0)
                        {
                            string pets = "";
                            foreach (Pet p in petowners.ElementAt(i - 1).Pets)
                            {
                                pets += $"{p.Name}({p.PetType.TypeName},{p.Race}, {p.Color}), ";
                            }

                            worksheet.Cells[i + 1, 9].Value = pets.Remove(pets.Length - 2);
                        }
                    }
                    worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();
                    worksheet.Row(1).Style.Font.Bold = true;

                    fileContents = package.GetAsByteArray();
                }

                if (fileContents == null || fileContents.Length == 0)
                {
                    return NotFound();
                }

                return File(
                    fileContents: fileContents,
                    contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    fileDownloadName: $"Export-Klijenti-{DateTime.Now.ToString("(dd.MM.yyyy)")}.xlsx"
                );
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }


        [HttpGet("{username}")]
        public IActionResult QRCode(string username)
        {
            try
            {
                SinglePetOwnerViewModel povm = new SinglePetOwnerViewModel();
                IPetOwnerLogic petOwnerLogic = new PetOwnerLogic(_context);

                povm.PetOwner = petOwnerLogic.GetByUserName(username) ?? throw new Exception($"Klijent sa korisničkim imenom \"{username}\" ne postoji");

                string vCardText = "BEGIN:VCARD\r\nVERSION:4.0\r\n";

                vCardText += $"N;CHARSET=utf8:{povm.PetOwner.FirstName} {povm.PetOwner.LastName}\r\n";

                if(povm.PetOwner.Email != "" && povm.PetOwner.Email != null)
                    vCardText += $"EMAIL:{povm.PetOwner.Email}\r\n";

                vCardText += $"TEL; HOME; VOICE:{povm.PetOwner.PhoneNumber}\r\n";

                vCardText += "END:VCARD";

                QRCodeData qrCodeData = new QRCodeGenerator().CreateQrCode(vCardText, QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);

                MemoryStream stream = new MemoryStream();
                qrCodeImage.Save(stream, System.Drawing.Imaging.ImageFormat.Bmp);

                povm.QRCode = stream.ToArray(); ;

                return View(povm);
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { ErrorMessage = ex.Message });
            }
        }


    }
}