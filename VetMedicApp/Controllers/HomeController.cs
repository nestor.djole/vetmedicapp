﻿using Microsoft.AspNetCore.Mvc;

namespace VetMedicApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult ToDo()
        {
            return View();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Analysis()
        {
            return View();
        }

        public IActionResult NotSignedIn()
        {
            return View("NotSignedIn");
        }

        public IActionResult UnauthorizedAccess()
        {
            return View("UnauthorizedAccess");
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}
